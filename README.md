# Datalad with GitLab LFS special remote

```bash
datalad create -c text2git datalad-gitlab-lfs
cd datalad-gitlab-lfs
head -c 100 /dev/urandom >random.bin
datalad save -m "data: add random data" random.bin
# next steps needs setup of gitlab.com with a token in ~/.python-gitlab.cfg
datalad create-sibling-gitlab --site gitlab.com --project iimog/datalad-gitlab-lfs --access ssh --name gitlab -d .
datalad push --to gitlab
git annex initremote gitlab-lfs type=git-lfs url=git@gitlab.com:iimog/datalad-gitlab-lfs.git encryption=none embedcreds=no
datalad push --to gitlab
git annex copy --to=gitlab-lfs random.bin
```

This results in an error:
```
copy random.bin (to gitlab-lfs...) 
                                  
  HttpExceptionRequest Request {
    host                 = "gitlab.com"
    port                 = 443
    secure               = True
    requestHeaders       = [("Authorization","<REDACTED>"),("Content-Type","application/octet-stream"),("Transfer-Encoding","chunked"),("User-Agent","git-annex/8.20210903-ga4d179c")]
    path                 = "/iimog/datalad-gitlab-lfs.git/gitlab-lfs/objects/346459c0e0eecae50b2628148c25e0828845ac871d079d08108dc87493272648/100"
    queryString          = ""
    method               = "PUT"
    proxy                = Nothing
    rawBody              = False
    redirectCount        = 10
    responseTimeout      = ResponseTimeoutDefault
    requestVersion       = HTTP/1.1
  }
   (StatusCodeException (Response {responseStatus = Status {statusCode = 400, statusMessage = "Bad Request"}, responseVersion = HTTP/1.1, responseHeaders = [("Server","cloudflare"),("Date","Fri, 29 Oct 2021 12:17:28 GMT"),("Content-Type","text/html"),("Content-Length","155"),("Connection","close"),("CF-RAY","6a5c57474f35c2a9-FRA")], responseBody = (), responseCookieJar = CJ {expose = []}, responseClose' = ResponseClose}) "<html>\r\n<head><title>400 Bad Request</title></head>\r\n<body>\r\n<center><h1>400 Bad Request</h1></center>\r\n<hr><center>cloudflare</center>\r\n</body>\r\n</html>\r\n")
                                  
                                  
  HttpExceptionRequest Request {
    host                 = "gitlab.com"
    port                 = 443
    secure               = True
    requestHeaders       = [("Authorization","<REDACTED>"),("Content-Type","application/octet-stream"),("Transfer-Encoding","chunked"),("User-Agent","git-annex/8.20210903-ga4d179c")]
    path                 = "/iimog/datalad-gitlab-lfs.git/gitlab-lfs/objects/346459c0e0eecae50b2628148c25e0828845ac871d079d08108dc87493272648/100"
    queryString          = ""
    method               = "PUT"
    proxy                = Nothing
    rawBody              = False
    redirectCount        = 10
    responseTimeout      = ResponseTimeoutDefault
    requestVersion       = HTTP/1.1
  }
   (StatusCodeException (Response {responseStatus = Status {statusCode = 400, statusMessage = "Bad Request"}, responseVersion = HTTP/1.1, responseHeaders = [("Server","cloudflare"),("Date","Fri, 29 Oct 2021 12:17:28 GMT"),("Content-Type","text/html"),("Content-Length","155"),("Connection","close"),("CF-RAY","6a5c5749f9edd70d-FRA")], responseBody = (), responseCookieJar = CJ {expose = []}, responseClose' = ResponseClose}) "<html>\r\n<head><title>400 Bad Request</title></head>\r\n<body>\r\n<center><h1>400 Bad Request</h1></center>\r\n<hr><center>cloudflare</center>\r\n</body>\r\n</html>\r\n")
failed                            
(recording state in git...)       
copy: 1 failed
```
